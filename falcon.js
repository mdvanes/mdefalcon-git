/*
Falcon

http://www.bit-101.com/tutorials/perspective.html

TODO fix at 25fps - https://github.com/mrdoob/three.js/issues/642
TODO log score
TODO offline - html5 localstorage
TODO define 3d borders (a group of meshes) -> or a grid!
TODO fix roll ball

this page: http://goo.gl/xGe3d

https://github.com/mrdoob/three.js/wiki/Using-SketchUp-Models
http://sketchup.google.com/3dwarehouse/details?mid=4fd08c8cf1a596c46dd84bc138e22252&prevstart=0

// Grid
var geometry = new THREE.Geometry();
geometry.vertices.push( new THREE.Vertex( new THREE.Vector3( - 500, 0, 0 ) ) );
geometry.vertices.push( new THREE.Vertex( new THREE.Vector3( 500, 0, 0 ) ) );
var material = new THREE.LineBasicMaterial( { color: 0xffffff, opacity: 0.2 } );
for ( var i = 0; i <= 10; i ++ ) {
	var line = new THREE.Line( geometry, material );
	line.position.y = - 120;
	line.position.z = ( i * 100 ) - 500;
	scene.add( line );
	var line = new THREE.Line( geometry, material );
	line.position.x = ( i * 100 ) - 500;
	line.position.y = - 120;
	line.rotation.y = 90 * Math.PI / 180;
	scene.add( line );
}

// Calculating Rotation Speed
Assume speedFactor = 2

dragSpeed = falcon.level * falcon.speedFactor;

The radius of the sphere is 10
the circumference of the sphere is 2*PI*r = 20*PI

At level = 1 
-> dragSpeed = 2
-> every frame a distance of 2 pixels is travelled
-> it takes 31 frames to travel 20*PI at a dragSpeed of 2 ( 20*PI / 2, with a drag speed of 2), so it should take 31 frames to rotate the sphere completely
-> rotationSpeed should be: 2*PI / 31 or 2*PI / (20*PI / dragspeed)

-> divide dragspeed by 10???!

*/
var falcon = {
	canvasWidth: 768,
	canvasHeight: 690,
	showDebug: true,
	level: 1,
	speedFactor: 0.1,
	direction: "up",
	state: "loading",
	turtleMoveMode: "roll", // "drag"
	bootstrap: function() {
		var camera, scene, renderer, stats;
		var debugWrapper;
		var turtleMesh;
		falcon.generateHtmlElements();
		falcon.initAnimation();
		falcon.animate();
	},
	/*
	Add buttons and such to the page.
	*/
	generateHtmlElements: function() {
		$("body").append("<div id='falcon'><input type='button' id='left'/><input type='button' id='right'/>");

		$("#falcon #left").attr("value","links");
		$("#falcon #left").mouseup( function(){
			falcon.setNewDirection( "left" );
			turtleMesh.rotation.x = 0;
			turtleMesh.rotation.y = 0;
			turtleMesh.rotation.z += (Math.PI / 2);
		});


		$("#falcon #right").attr("value","rechts");
		$("#falcon #right").mouseup( function(){
			falcon.setNewDirection( "right" );
			turtleMesh.rotation.x = 0;
			turtleMesh.rotation.y = 0;
			turtleMesh.rotation.z -= (Math.PI / 2);
		});

		// Menu
		menuWrapper = document.createElement( "div" );
		menuWrapper.id = "menuWrapper";
		$("#falcon").append( menuWrapper );

		// Start button
		$("#falcon #menuWrapper").append("<input type='button' id='start' value='start'/>");
		$("#falcon #start").mouseup( function() {
			falcon.state = "started";
		});

		// Debug
		if( falcon.showDebug )
		{
			debugWrapper = document.createElement( "div" );
			debugWrapper.id = "debugWrapper";
			$("#falcon").append( debugWrapper );

			// Speed selector
			$("#falcon #debugWrapper").append("<select id='level'>" + 
				"<option value='1'>level1</option>" + 
				"<option value='2'>level2</option>" +
				"<option value='3'>level3</option>" +
				"<option value='4'>level4</option>" +
				"<option value='5'>level5</option>" +
				"<option value='6'>level6</option>" + 
				"<option value='7'>level7</option>" +
				"<option value='8'>level8</option>" +
				"<option value='9'>level9</option>" +
				"<option value='10'>level10</option>" +
				"</select>");
			$("#falcon #level").change( function() {
				falcon.level = $(this).val();
			});
		}
		
		// Hotkeys
		$("body").keyup( function(event){
			//console.log( "pressed key="+event.keyCode ); // TODO remove
			var arrowleft = 37, arrowright = 39, spacebar = 32;
			if( event.keyCode == arrowleft ) {
				$("#falcon #left").mouseup();
				event.preventDefault();
			} else if ( event.keyCode == arrowright ) {
				$("#falcon #right").mouseup();
				event.preventDefault();
			} else if ( event.keyCode == spacebar ) {
				$("#falcon #start").mouseup();
				event.preventDefault();
			}
		});
	},
	setNewDirection: function( turn ) {
		var directions = ["up","right","down","left"];
		var currentDirectionIndex = $.inArray( falcon.direction, directions );
		var newDirectionIndex = 0;
		if( turn == "left" ) {
			newDirectionIndex = currentDirectionIndex - 1;
		} else {
			newDirectionIndex = currentDirectionIndex + 1;
		}
		if( newDirectionIndex < 0 ) {
			// If going up and turning left, go left
			falcon.direction = directions[4];
		} else if ( newDirectionIndex >= directions.length ) {
			// If going left and turning right, go up
			falcon.direction = directions[0];
		} else {
			falcon.direction = directions[newDirectionIndex];
		}
	},
	initAnimation: function() {
		// create the Scene
		scene = new THREE.Scene();

		var VIEW_ANGLE = 45,
		    ASPECT = falcon.canvasWidth / falcon.canvasHeight,
		    NEAR = 0.1,
		    FAR = 10000;

		camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR );

		// the camera starts at 0,0,0 so pull it back
		camera.position.z = 400;
		scene.add(camera);

		// TODO add WebGL support?
		renderer = new THREE.CanvasRenderer();
		renderer.setSize( falcon.canvasWidth, falcon.canvasHeight );

		// Create the wrapper element for the canvas
		var wrapper = document.createElement( "div" );
		wrapper.id = "canvasWrapper";
		$("#falcon").append( wrapper );
		$(wrapper).append( renderer.domElement );
		
//		turtleMesh = falcon.getMorgalMesh();
		turtleMesh = falcon.getSphereMesh();
		//turtleMesh.scale.set( 0.1, 0.1, 0.1 );
		scene.add( turtleMesh );

		/*
		var loader = new THREE.ColladaLoader();
//		loader.load('tron.dae', function (result) {
		loader.load('yellowbike.dae', function (result) {
//		loader.load('hayabusa.dae', function (result) {
			turtleMesh = result.scene;
			turtleMesh.rotation.x = -(Math.PI / 2);
//			turtleMesh.scale.set( 0.3, 0.3, 0.3 );
			scene.add( turtleMesh );
		});
		*/
				
		if( falcon.showDebug )
		{
			// Init the Stats and append it to the Dom - performance vuemeter
			stats = new Stats();
			$(debugWrapper).append( stats.domElement );
		}
	},
	animate: function() {
		// Relaunch the 'timer'
		requestAnimationFrame( falcon.animate );

		// Render the 3D scene
		falcon.render();

		if( falcon.showDebug )
		{
			stats.update();
		}
	},
	render: function() {
		// Updates that should be done each frame of the animation
		
		// Updates that should only be done if the state of the application is started
		if( falcon.state == "started" )
		{
			var dragSpeed = falcon.level * falcon.speedFactor;
			var rollSpeed = falcon.level * (falcon.speedFactor / 10);
			
			if( falcon.direction=="up" && turtleMesh.position.y < 160 ) {
				// TODO remove - console.log( turtleMesh.position.y + " " + (768 / 2) );
				turtleMesh.position.y += dragSpeed;
				if( falcon.turtleMoveMode == "roll" ) {
					turtleMesh.rotation.x -= rollSpeed;
				}
			} else if ( falcon.direction=="right" && turtleMesh.position.x < 180 ) {
				turtleMesh.position.x += dragSpeed;
				if( falcon.turtleMoveMode == "roll" ) {
					turtleMesh.rotation.y += rollSpeed;
				}
			} else if ( falcon.direction=="down" && turtleMesh.position.y > -160 ) {
				turtleMesh.position.y -= dragSpeed;
				if( falcon.turtleMoveMode == "roll" ) {
					turtleMesh.rotation.x += rollSpeed;
				}
			} else if ( falcon.direction=="left" && turtleMesh.position.x > -180 ) {
				turtleMesh.position.x -= dragSpeed;
				if( falcon.turtleMoveMode == "roll" ) {
					turtleMesh.rotation.y -= rollSpeed;
				}
			} else {
				alert("Buitenspel!");
				top.location.reload();
			}
		}
		
		// Actually display the scene in the Dom element
		renderer.render( scene, camera );
	},
	getSphereMesh: function() {
		// TODO clean up
		// SphereGeometry radius, segmentsWidth, segmentsHeight
		var geometry = new THREE.SphereGeometry( 10, 10, 10, false );
		var material = new THREE.MeshBasicMaterial( { color: 0x00ffff, wireframe: true } );
		for ( var i = 0, l = geometry.faces.length; i < l; i ++ ) {
		var face = geometry.faces[ i ];
		//if ( Math.random() > 0.7 ) face.material = [ materials[ Math.floor( Math.random() * materials.length ) ].material ];
		face.material = material;
		}
		//materials.push( { material: new THREE.MeshFaceMaterial(), overdraw: false, doubleSided: true } );
		//objects = [];
		//for ( var i = 0, l = materials.length; i < l; i ++ ) {
		var sphere = new THREE.Mesh( geometry, material );
		sphere.doubleSided = true;
		return sphere;
	},
	getMorgalMesh: function()	{
		var extrudeSettings = {	amount: 100, bevelEnabled: false, bevelSegments: 1, steps: 2 };

		var morgalShape = new THREE.Shape();

		morgalShape.moveTo( 0, 0 );
		morgalShape.lineTo( 0, 10 );
		morgalShape.lineTo( -50, 50 );
		morgalShape.lineTo( -75, -50 );
		morgalShape.lineTo( 0, -10 );

		morgalShape.lineTo( 75, -50 );
		morgalShape.lineTo( 50, 50 );
		morgalShape.lineTo( 1, 10 );

		morgalShape.lineTo( 1, 0 );
		morgalShape.lineTo( 45, 30 );
		morgalShape.lineTo( 60, -30 );
		morgalShape.lineTo( 0, -1 );
		morgalShape.lineTo( -60, -30 );
		morgalShape.lineTo( -45, 30 );

		morgalShape.lineTo( 0, -0 );

		var morgal3d = morgalShape.extrude( extrudeSettings );

		var morgal = new THREE.Mesh( 
			morgal3d,
			new THREE.MeshNormalMaterial() );
		return morgal;
	}
}